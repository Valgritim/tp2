<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
      <link rel="stylesheet" href="css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Play&display=swap" rel="stylesheet"> 
    <title>Formulaire d'ajout d'une adresse</title>
</head>
<body class="bg-gra-01">
	<div class="container" style="margin-top:50px; width:50%;">
	
		<c:if test="${verif eq false}">
			<div class='alert alert-danger mt-5'><c:out value="${message}"/></div>		
		</c:if>
	
		<fieldset>
		<legend>Créer une adresse</legend>
			<form method="post" action="creationAdresse">
			  <c:import url="/WEB-INF/inc/formulaire.jsp"/>
			  <div class="form-group">
			    <label for="rue" >rue</label>		    
			      <input type="text"  class="form-control" id="rue" name="rue" value="" placeholder="votre rue">		   
			  </div>
			  <div class="form-group">
			    <label for="cp" >code postal</label>		    
			      <input type="text"  class="form-control" id="cp" name="cp" value="" placeholder="votre code postal">		   
			  </div>
			  <div class="form-group">
			    <label for="ville" >Ville</label>		    
			      <input type="text"  class="form-control" id="ville" name="ville" value="" placeholder="votre ville">		   
			  </div>
			  <input type="submit" class="btn btn-primary" value="Ajouter">
			</form>
		</fieldset>
	</div>
	<script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
</html>