<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <link rel="stylesheet" href="css/style.css">
  <link href="https://fonts.googleapis.com/css?family=Play&display=swap" rel="stylesheet"> 
<title>Formulaire d'ajout de client</title>
</head>
<body>
	<div class="container mt-5" style="width:50%; margin-top:50px;" >
		<c:if test="${verif eq false}">
			<div class='alert alert-danger mt-5'><c:out value="${message}"/></div>		
		</c:if>
	
		<fieldset>
		<legend>Créer un client</legend>
			<form method="post" action="creationClient">	
			<c:import url="/WEB-INF/inc/formulaire.jsp"/>
				<input type="submit" class="btn btn-primary" value="Ajouter">
			</form>
		</fieldset>
	</div>
	<script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
</html>