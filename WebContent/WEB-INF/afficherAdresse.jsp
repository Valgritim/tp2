<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import = "org.eclipse.model.*" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
  <link href="https://fonts.googleapis.com/css?family=Play&display=swap" rel="stylesheet"> 
<title>Affichage de l'adresse</title>
</head>
<body>
	<div class="container">
	
		<div class="alert alert-success mt-5"><c:out value="${message}"/></div>	
			<div class="card" style="width:25rem">	  
			  <div class="card-body">
			    <h5 class="card-title">Fiche adresse</h5>			    
			  </div>
			  <ul class="list-group list-group-flush">
			    <li class="list-group-item">Nom du client: <c:out value="${adresse.client.nom}"/></li>
			    <li class="list-group-item">Prénom du client: <c:out value="${adresse.client.prenom}"/></li>
			    <li class="list-group-item">Tel du client: <c:out value="${adresse.client.telephone}"/></li>
			    <li class="list-group-item">Rue du client: <c:out value="${adresse.rue}"/></li>
			    <li class="list-group-item">CP du client: <c:out value="${adresse.codePostal}"/></li>
			    <li class="list-group-item">Ville du client: <c:out value="${adresse.ville}"/></li>			    
			  </ul>
			</div>			
	</div>
	<script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
</html>