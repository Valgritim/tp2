<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
	<title>Insert title here</title>
</head>
<body>
	<h1>Menu</h1>
	<c:url value="/creationClient" var="lienCLient" />
	<c:url value="/creationAdresse" var="lienAdresse" />
	<a href="${lienCLient}">Lien vers le formulaire de création client</a><br/>
	<a href="${lienAdresse}">Lien vers le formulaire de création adresse</a>

</body>
</html>