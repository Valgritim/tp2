package org.eclipse.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.model.Client;

/**
 * Servlet implementation class CreationClientServlet
 */
@WebServlet("/creationClient")
public class CreationClientServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static final String VUE_SUCCESS = "/WEB-INF/afficherClient.jsp";
	public static final String VUE_FORM = "/WEB-INF/creerClient.jsp";

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
				
		this.getServletContext().getRequestDispatcher("/WEB-INF/creerClient.jsp").forward(request, response);	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		String nom = request.getParameter("nom");
		String prenom = request.getParameter("prenom");
		String telephone = request.getParameter("phone");		
		Client client = new Client(nom,prenom,telephone);
		boolean verif = true;
		if (nom.trim().isEmpty() || prenom.trim().isEmpty() || telephone.isEmpty()) {
			String message = "Erreur - Vous n'avez pas rempli tous les champs obligatoires.";
			verif = false;
			request.setAttribute("verif", verif);
			request.setAttribute("message", message);
			this.getServletContext().getRequestDispatcher(VUE_FORM).forward(request, response);
		} else {
			String message = "Client cr�� avec succ�s !";			
			request.setAttribute("message", message);
			request.setAttribute("client", client);
			this.getServletContext().getRequestDispatcher(VUE_SUCCESS).forward(request, response);
		}
		
	}

}

