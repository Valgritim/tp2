package org.eclipse.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.model.Adresse;
import org.eclipse.model.Client;

/**
 * Servlet implementation class CreationAdresseServlet
 */
@WebServlet("/creationAdresse")
public class CreationAdresseServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static final String VUE_SUCCESS = "/WEB-INF/afficherAdresse.jsp";
	public static final String VUE_FORM = "/WEB-INF/creerAdresse.jsp";

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		this.getServletContext().getRequestDispatcher(VUE_FORM).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		String nom = request.getParameter("nom");
		String prenom = request.getParameter("prenom");
		String telephone = request.getParameter("phone");
		Client client = new Client(nom,prenom,telephone);
		
		String rue = request.getParameter("rue");
		String codePostal = request.getParameter("cp");
		String ville = request.getParameter("ville");
		Adresse adresse = new Adresse(rue,codePostal,ville,client);
		boolean verif = true;
		
		if (nom.trim().isEmpty() || prenom.trim().isEmpty()|| telephone.isEmpty() || rue.isEmpty() || codePostal.isEmpty() || ville.isEmpty()) {
			verif = false;
			request.setAttribute("verif", verif);
			String message = "Erreur - Vous n'avez pas rempli tous les champs obligatoires.";
			request.setAttribute("message", message);
			this.getServletContext().getRequestDispatcher(VUE_FORM).forward(request, response);
		} else {
			String message = "Adresse creee avec succes !";
			request.setAttribute("adresse", adresse);
			request.setAttribute("message", message);
			
			this.getServletContext().getRequestDispatcher(VUE_SUCCESS).forward(request, response);
		}

		
	}

}
